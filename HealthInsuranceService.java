package com.emids.healthInsurance;

public class HealthInsuranceService {

	public int calculateHealthInsurance(DetailsBean detailsBean)
	{

		int premium= 5000;

		if(detailsBean.getAge()<18) {
			premium=5000;
		}
		else if(detailsBean.getAge()>=18 || detailsBean.getAge()<=40)
		{
			premium=(int)(premium+(premium*0.1));
		}
		else if(detailsBean.getAge()>40)
		{
			premium=(int)(premium+(premium*0.2));
		}

		if(detailsBean.getGender().equalsIgnoreCase("Male"))
		{
			premium=(int)(premium+(premium*0.2));
		}


		if(detailsBean.getHyperTension().equalsIgnoreCase("Yes") || 
				detailsBean.getBloodPressure().equalsIgnoreCase("Yes") ||
				detailsBean.getBloodSugar().equalsIgnoreCase("Yes") ||
				detailsBean.getOverWeight().equalsIgnoreCase("Yes"))
		{
			int count=0;
			String values[]= {detailsBean.getBloodPressure(),detailsBean.getBloodSugar(),detailsBean.getHyperTension()
					,detailsBean.getOverWeight()};
			
			for(int i=0; i<values.length;i++)
			{
				if(values[i].equalsIgnoreCase("Yes"))
				{
					count++;
				}
			}
			
			premium=premium+(premium*count/100);

		}
		
		if(detailsBean.getDailyExercise().equalsIgnoreCase("Yes"))
		{
			premium=(int)(premium-(premium*0.03));
		}
		
		if(detailsBean.getSmoking().equalsIgnoreCase("Yes")||
				detailsBean.getAlcohol().equalsIgnoreCase("Yes") ||
				detailsBean.getDrugs().equalsIgnoreCase("Yes"))
		{
			
			int count=0;
			String values[]= {detailsBean.getSmoking(),detailsBean.getAlcohol(),detailsBean.getDrugs()};
			
			for(int i=0; i<values.length;i++)
			{
				if(values[i].equalsIgnoreCase("Yes"))
				{
					count++;
				}
			}
			premium=premium+(premium*count/100);
		}

		return premium;
	}

}
