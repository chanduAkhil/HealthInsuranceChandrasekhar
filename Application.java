package com.emids.healthInsurance;

import java.util.Scanner;

public class Application {

	public static void main(String[] args) {
		
		Scanner scannerObj =new Scanner(System.in);
		
		DetailsBean detailsBean=new DetailsBean();
		
		System.out.println("Name :");
		String personName= scannerObj.next();
		detailsBean.setName(personName);
		
		System.out.println("Gender");
		String personGender= scannerObj.next();
		detailsBean.setGender(personGender);
		
		System.out.println("Age");
		int personAge= scannerObj.nextInt();
		detailsBean.setAge(personAge);
		
		System.out.println("Hypertension Yes/No:");
		String hyperTension= scannerObj.next();
		detailsBean.setHyperTension(hyperTension);
		
		System.out.println("BloodPressure Yes/No :");
		String bloodPressure= scannerObj.next();
		detailsBean.setBloodPressure(bloodPressure);
		
		System.out.println("BloodSugar Yes/No :");
		String bloodSugar= scannerObj.next();
		detailsBean.setBloodSugar(bloodSugar);
		
		System.out.println("overWeight Yes/No :");
		String overWeight= scannerObj.next();
		detailsBean.setOverWeight(overWeight);
		
		System.out.println("smoking Yes/No :");
		String smoking= scannerObj.next();
		detailsBean.setSmoking(smoking);
		
		System.out.println("Alcohol Yes/No :");
		String alcohol= scannerObj.next();
		detailsBean.setAlcohol(alcohol);
		
		System.out.println("Daily Exercise Yes/No :");
		String dailyExercise= scannerObj.next();
		detailsBean.setDailyExercise(dailyExercise);
		
		System.out.println("Drugs Yes/No :");
		String drugs= scannerObj.next();
		detailsBean.setDrugs(drugs);
		
		
	int premuim=new HealthInsuranceService().calculateHealthInsurance(detailsBean);
		
		System.out.println("Health Insurance Premium for"+detailsBean.getName()+ " "+premuim);


	}

}